## [Silentte.cc](https://silentte.cc)
⭐ A very simple landing page, originally for Silenttecc.

## Usage
The site is licensed under CC BY. In laymans terms, use the site for whatever - just make sure to give appropriate credit and cite this repo as the source. More info can be found in [LICENSE.md](LICENSE.md) or on the [Creative Commons' website](https://creativecommons.org/licenses/by/4.0/).

## Dependencies 
The website makes use of many cool open-source frameworks and software:
- [Animate.css](https://daneden.github.io/animate.css/) provides flashy animations
- [Bulma](https://bulma.io) is my SS framework of choice
- [Font Awesome](https://fontawesome.com/) are cool icons
